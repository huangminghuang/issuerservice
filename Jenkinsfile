
final appName = 'issuer'
final nodeLabel = "${appName}-${env.BRANCH_NAME}-${env.BUILD_NUMBER}"

def notifyFailed() {
//   slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    emailext (
        subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: """<p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
            <p>Check console output at "<a href="${env.BUILD_URL}">${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>"</p>""",
        recipientProviders: [[$class: 'CulpritsRecipientProvider'],[$class: 'DevelopersRecipientProvider']]
    )
}

podTemplate(label: nodeLabel,
    containers: [
        containerTemplate(
            name: 'jnlp',
            image: 'huangminghuang/jnlp-slave-docker',
            args:  '${computer.jnlpmac} ${computer.name}'
        ),
        containerTemplate(
            name: 'gcloud',
            image: 'google/cloud-sdk:178.0.0-alpine',
            ttyEnabled: true,
            command: 'cat'
        ),
        containerTemplate(
            name: 'kubectl',
            image: 'lachlanevenson/k8s-kubectl:v1.7.10',
            ttyEnabled: true,
            command: 'cat'
        ),
    ],
    volumes: [
        hostPathVolume(
            hostPath: '/var/run/docker.sock',
            mountPath: '/var/run/docker.sock'
        ),
        hostPathVolume(
            hostPath: '/usr/bin/docker',
            mountPath: '/usr/bin/docker'
        )
    ]
) {
    node (nodeLabel) {
        try {
            final scmVars = checkout scm

            def imageTags = [
                    "${env.DEFAULT_CONTAINER_REGISTRY}/${appName}:${env.BRANCH_NAME}",
                    "${env.DEFAULT_CONTAINER_REGISTRY}/${appName}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
                ]

            stage('Build') {
                echo 'Building docker image...'
                docker.build("${appName}:${scmVars.GIT_COMMIT}")
            }

            stage('Test') {
                echo 'Running tests...'
                sh("docker run --rm ${appName}:${scmVars.GIT_COMMIT} go test")
            }

            stage('Push') {

                container('gcloud') {
                    echo 'Push image to GCR'
                    imageTags.each {
                        sh("docker tag ${appName}:${scmVars.GIT_COMMIT} ${it}")
                        sh("gcloud docker -- push ${it}")
                    }
                }
            }
            stage('Finalize') {
                withCredentials([usernamePassword(credentialsId: 'Bitbucket-credential', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                    bitbucketTagCommit(username: GIT_USERNAME, password: GIT_PASSWORD, tag: "${env.BRANCH_NAME}.${env.BUILD_NUMBER}" )
                }

                currentBuild.description = """
                                           image_tag: "${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
                                           image_repo: "${env.DEFAULT_CONTAINER_REGISTRY}/${appName}"
                                           """.stripIndent()
            }
        } catch (e) {
            currentBuild.result = "FAILED"
            notifyFailed()
            throw e
        }
    }
}
