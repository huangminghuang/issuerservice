package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	config "bitbucket.org/objectcomputing/issuerservice/config"
	pb "bitbucket.org/objectcomputing/issuerservice/walletpb"
	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var conf = &config.Configuration{
	HTTPPort: 8080,
	GRPCPort: 9090,
	IssuerID: "test",
	Customers: []*config.Customer{
		&config.Customer{
			UID: "johndoe@gmail.com",
			Wallets: []*config.Wallet{
				&config.Wallet{
					LastName:      "Doe",
					FirstName:     "John",
					StreetAddress: "10000 Olive Blvd",
					City:          "St. Louis",
					StateProvince: "MO",
					PostalCode:    "63141",
					CountryName:   "USA",
					CardNumber:    "1234567890"},
			},
		},
		&config.Customer{
			UID: "janedoe@gmail.com",
			Wallets: []*config.Wallet{
				&config.Wallet{
					LastName:      "Doe",
					FirstName:     "Jane",
					StreetAddress: "20000 Olive Blvd",
					City:          "St. Louis",
					StateProvince: "MO",
					PostalCode:    "63137",
					CountryName:   "USA",
					CardNumber:    "2345678901"},
				&config.Wallet{
					LastName:      "Doe",
					FirstName:     "Jane",
					StreetAddress: "20000 Olive Blvd",
					City:          "St. Louis",
					StateProvince: "MO",
					PostalCode:    "63137",
					CountryName:   "USA",
					CardNumber:    "3456789012"},
			},
		},
	},
}

func runServers() <-chan error {
	ch := make(chan error, 2)
	go func() {
		if err := Run(conf); err != nil {
			ch <- fmt.Errorf("cannot run gateway service: %v", err)
		}
	}()
	return ch
}

func TestMain(m *testing.M) {
	flag.Parse()

	errCh := runServers()

	ch := make(chan int, 1)
	go func() {
		time.Sleep(100 * time.Millisecond)
		ch <- m.Run()
	}()

	select {
	case err := <-errCh:
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	case status := <-ch:
		os.Exit(status)
	}
}

func TestGRPCGet(t *testing.T) {
	testGRPCGet(conf.Customers[0], t)
	testGRPCGet(conf.Customers[1], t)
}

func testGRPCGet(cust *config.Customer, t *testing.T) {
	address := fmt.Sprintf("localhost:%v", conf.GRPCPort)

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		t.Errorf("grpc.Dial(%q) failed with %v; want success", address, err)
		return
	}
	defer conn.Close()
	c := pb.NewIssuerClient(conn)

	request := &pb.WalletRequest{TransactionId: "abc", Uid: cust.UID}

	reply, err := c.Get(context.Background(), request)
	if err != nil {
		t.Errorf("Fail to get wallet: %v", err)
		return
	}

	verifyReply(request.TransactionId, cust, reply, t)
}

func TestHttpGet(t *testing.T) {
	testHTTPGet(conf.Customers[0], t)
	testHTTPGet(conf.Customers[1], t)
}

func testHTTPGet(cust *config.Customer, t *testing.T) {
	const transactionID = "abc"

	url := fmt.Sprintf("http://localhost:%v/v1/wallets/transaction_id/%v/uid/%v", conf.HTTPPort, transactionID, cust.UID)
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("http.Get(%q) failed with %v; want success", url, err)
		return
	}
	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("iotuil.ReadAll(resp.Body) failed with %v; want success", err)
		return
	}

	if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Errorf("resp.StatusCode = %d; want %d", got, want)
		return
	}

	var msg pb.WalletReply
	if err := jsonpb.UnmarshalString(string(buf), &msg); err != nil {
		t.Errorf("jsonpb.UnmarshalString(%s, &msg) failed with %v; want success", buf, err)
		return
	}

	verifyReply(transactionID, cust, &msg, t)

	const contentType = "application/json"

	if value := resp.Header.Get("Content-Type"); value != contentType {
		t.Errorf("Content-Type was %s, wanted %s", value, contentType)
	}
}

func verifyReply(transactionID string, cust *config.Customer, msg *pb.WalletReply, t *testing.T) {
	if got, want := msg.Uid, cust.UID; got != want {
		t.Errorf("msg.Id = %q; want %q", got, want)
	}

	if got, want := msg.TransactionId, transactionID; got != want {
		t.Errorf("msg.TransactionId = %q; want %q", got, want)
	}

	if got, want := len(msg.Wallets), len(cust.Wallets); got != want {
		t.Errorf("len(msg.Wallets) = %v; want %v", got, want)
	}

	for i := range cust.Wallets {
		gotWallet := msg.Wallets[i]
		wantWallet := cust.Wallets[i]

		if got, want := gotWallet.FirstName, wantWallet.FirstName; got != want {
			t.Errorf("%v wallet[%v].FirstName = %q; want %q", msg.Uid, i, got, want)
		}

		if got, want := gotWallet.LastName, wantWallet.LastName; got != want {
			t.Errorf("%v wallet[%v].LastName = %q; want %q", msg.Uid, i, got, want)
		}

		if got, want := gotWallet.StreetAddress, wantWallet.StreetAddress; got != want {
			t.Errorf("%v wallet[%v].StreetAddress = %q; want %q", msg.Uid, i, got, want)
		}

		if got, want := gotWallet.City, wantWallet.City; got != want {
			t.Errorf("%v wallet[%v].City = %q; want %q", msg.Uid, i, got, want)
		}
	}
}

func TestHttpGetReturnsNotFound(t *testing.T) {
	const transactionID = "ABC"

	url := fmt.Sprintf("http://localhost:%v/v1/wallets/transaction_id/%v/uid/%v", conf.HTTPPort, transactionID, "non-existant-id")
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("http.Post(%q) failed with %v; want success", url, err)
		return
	}
	defer resp.Body.Close()

	if got, want := resp.StatusCode, http.StatusNotFound; got != want {
		t.Errorf("resp.StatusCode = %d; want %d", got, want)
		return
	}
}

func TestGRPCGetReturnsNotFound(t *testing.T) {
	address := fmt.Sprintf("localhost:%v", conf.GRPCPort)

	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		t.Errorf("grpc.Dial(%q) failed with %v; want success", address, err)
		return
	}
	defer conn.Close()
	c := pb.NewIssuerClient(conn)

	request := &pb.WalletRequest{TransactionId: "abc", Uid: "non-existant-id"}

	_, err = c.Get(context.Background(), request)

	status, ok := status.FromError(err)

	if !ok || status.Code() != codes.NotFound {
		t.Errorf("IssuerService.Get did not return the expected codes.NotFound")
		return
	}
}

func TestHttpHealth(t *testing.T) {
	url := fmt.Sprintf("http://localhost:8080/health")
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("http.Get(%q) failed with %v; want success", url, err)
		return
	}

	if got, want := resp.StatusCode, http.StatusOK; got != want {
		t.Errorf("resp.StatusCode = %d; want %d", got, want)
		return
	}
}
